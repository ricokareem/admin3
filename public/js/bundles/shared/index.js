// Refresh Calendar Button
$('#refresh-button').click(function() {
    $('#calendar').fullCalendar('refetchEvents');
});

// Initialize Materialize Components
$('.button-collapse').sideNav();
$('.modal-trigger').leanModal();
