require('../../../less/lite.less');
require('../shared');


// Banners data array for filling in info box
var BannerListData = [];

// DOM Ready =============================================================
$(document).ready(function() {

    // Populate the banner table on initial page load
    populateTable();

    // Banner Title link click
    $('#bannerList table tbody').on('click', 'td a.linkshowbanner', showBannerInfo);

    // Add Banner button click
    $('#btnCreateBanner').on('click', createBanner);

    // Delete Banner link click
    $('#bannerList table tbody').on('click', 'td a.linkdeletebanner', deleteBanner);

});

// Functions =============================================================

// Fill table with data
function populateTable() {

    // Empty content string
    var tableContent = '';

    // jQuery AJAX call for JSON
    $.getJSON( '/banners/', function( data ) {

        BannerListData = data;

        // For each item in our JSON, add a table row and cells to the content string
        $.each(data, function(){
            tableContent += '<tr>';
            tableContent += '<td><a href="#" class="linkshowbanner" rel="' + this.title + '">' + this.title + '</a></td>';
            tableContent += '<td>' + this.greeting + '</td>';
            tableContent += '<td><a href="#" class="linkdeletebanner" rel="' + this._id + '">delete</a></td>';
            tableContent += '</tr>';
        });

        // Inject the whole content string into our existing HTML table
        $('#bannerList table tbody').html(tableContent);
    });
};

// Show Banner Info
function showBannerInfo(event) {

    // Prevent Link from Firing
    event.preventDefault();

    // Retrieve banner Title from link rel attribute
    var thisTitle = $(this).attr('rel');

    // Get Index of object based on id value
    var arrayPosition = BannerListData.map(function(arrayItem) { return arrayItem.title; }).indexOf(thisTitle);


    // Get our Banner Object
    var thisBannerObject = BannerListData[arrayPosition];

    //Populate Info Box
    $('#bannerInfoImage').attr('src',thisBannerObject.image_url);
    $('#bannerInfoTitle').text(thisBannerObject.title);
    $('#bannerInfoGreeting').text(thisBannerObject.greeting);
    $('#bannerInfoSeason').text(thisBannerObject.season);
    $('#bannerInfoStartDate').text(thisBannerObject.start);
    $('#bannerInfoEndDate').text(thisBannerObject.end);
    $('#bannerInfoTimePeriod').text(thisBannerObject.time_period);
    $('#bannerInfoImageID').text(thisBannerObject.image_id);
    $('#bannerInfoImageUrl').html('<a href='
        +thisBannerObject.image_url
        +' target=_blank>'+thisBannerObject.image_url+'</a>');
    $('#bannerInfoFontColor').text(thisBannerObject.font_color);

};

// Add Banner
function createBanner(event) {
    event.preventDefault();

    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    $('#createBanner input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });

    // Check and make sure errorCount's still at zero
    if(errorCount === 0) {

        // If it is, compile all banner info into one object
        var newBanner = {
            'title': $('#createBanner fieldset input#inputTitle').val(),
            'greeting': $('#createBanner fieldset input#inputGreeting').val(),
            'season': $('#createBanner fieldset input#inputSeason').val(),
            'start': $('#createBanner fieldset input#start').val(),
            'end': $('#createBanner fieldset input#end').val(),
            'time_period': $('#createBanner fieldset input#inputTimePeriod').val(),
            'image_id': $('#createBanner fieldset input#inputImageID').val(),
            'image_url': $('#createBanner fieldset input#inputImageUrl').val(),
            'font_color': $('#createBanner fieldset input#inputFontColor').val()
        }

        // Use AJAX to post the object to our createBanner service
        $.ajax({
            type: 'POST',
            data: newBanner,
            url: '/banners/create',
            dataType: 'JSON'
        }).done(function( response ) {

            // Check for successful (blank) response
            if (response.msg === '') {

                // Clear the form inputs
                $('#createBanner fieldset input').val('');

                // Update the table
                populateTable();

            }
            else {

                // If something goes wrong, alert the error message that our service returned
                alert('Error: ' + response.msg);

            }
        });
    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};

// Delete Banner
function deleteBanner(event) {

    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this banner?');

    // Check and make sure the banner confirmed
    if (confirmation === true) {

        // If they did, do our delete
        $.ajax({
            type: 'DELETE',
            url: '/banners/delete/' + $(this).attr('rel')
        }).done(function( response ) {

            // Check for a successful (blank) response
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }

            // Update the table
            populateTable();

        });

    }
    else {

        // If they said no to the confirm, do nothing
        return false;

    }

};
