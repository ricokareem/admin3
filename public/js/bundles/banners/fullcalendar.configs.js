$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    defaultView: 'month',
    eventColor: '#263238',
    eventOrder: ['title', 'start'],
    events: '/banners',
    editable: true,
    eventRender: function (event, element) {
        element.attr('href', 'javascript:void(0);');
        element.click(function() {
            $('#editBannerModal-'+ event._id).openModal();
        });
    }
});
