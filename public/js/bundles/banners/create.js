// New Banner Form - Save Button
    $('#new-image-form .save').click(function(event) {
        event.preventDefault();
        createBanner(event);
        // TODO: THERE IS DUPLICATE CODE IN CANCEL, FIX THIS
        $('#image-dropzone').addClass('show').removeClass('hide');
        $('#image-preview').addClass('hide').removeClass('show');
        // $('#current-images').load('/'+' #current-images>*','');
        // Dropzone.options.imageDropzone.init;
        location.reload();
    });

// New Banner Form - Cancel button
    $('#new-image-form .cancel-upload').click(function(e) {
        e.preventDefault();
        $('#image-dropzone').addClass('show').removeClass('hide');
        $('#image-preview').addClass('hide').removeClass('show');
        // Dropzone.options.imageDropzone.init;
        location.reload();
    });


// ADD BANNER
function createBanner(event) {
    event.preventDefault();

    // Super basic validation - increase errorCount variable if any fields are blank
    var errorCount = 0;
    $('#new-image-form input').each(function(index, val) {
        if($(this).val() === '') { errorCount++; }
    });

    // Check and make sure errorCount's still at zero
    if(errorCount === 0) {

        // If it is, compile all banner info into one object
        var newBanner = {
            'title': $('.dz-filename span').text() || '',
            'greeting': $('#new-image-form #greeting').val() || '',
            'season': $('#new-image-form #season').val() || '',
            'start': $('#new-image-form #start').val() || '',
            'end': $('#new-image-form #end').val() || '',
            'time_period': $('#new-image-form #time_period').val() || '',
            'image_id': $('#new-image-form #image_id').val() || '',
            'image_url': $('#new-image-form #image_url').val() || '',
            'font_color': $('#new-image-form #font_color').val() || ''
        }

        // Use AJAX post to insert new object into mongoDB
        $.ajax({
            type: 'POST',
            data: newBanner,
            url: '/banners/create',
            dataType: 'JSON'
        }).done(function( response ) {

            // Check for successful (blank) response
            if (response.msg === '') {
            } else {
                alert('Error: ' + response.msg);
            }

        });
    }
    else {
        // If errorCount is more than 0, error out
        alert('Please fill in all fields');
        return false;
    }
};

