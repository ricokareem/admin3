// Delete Banner button link
    $('#current-images').on('click', 'a.deletebanner', deleteBanner);

function deleteBanner(event) {
    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this banner?');

    // Check and make sure the banner confirmed
    if (confirmation === true) {
            bannerId = $(this).attr('rel');
            $.ajax({
                type: 'DELETE',
                url: '/banners/delete/' + bannerId
            }).done(function( response ) {

                // Check for a successful (blank) response
                if (response.msg === '') {
                }
                else {
                        alert('Error: ' + response.msg);
                }
                // Remove banner from the DOM
                $('.'+bannerId).remove();
                $('#calendar').fullCalendar('refetchEvents');
            });
    }
    else {
        return false;
    }
}
