require('./create.js');
require('./delete.js');
require('./fullcalendar.configs.js');
require('../lib/dropzone.configs.js');
require('../../../less/banners.less');
require('../shared');


(function () {

    var $dropzone  = $('#image-dropzone');
    var $refresh   = $('#refresh-button');

    $('#image-preview').addClass('hide');

    // Create Banner Button(+)
    $dropzone.on('click', function() {
        $('html, body').animate({
            scrollTop: $('#calendar-container').offset().top
        }, 2000);
    });

    // use jQuery datetimepicker instead of Materialize Datepicker
    // http://xdsoft.net/jqplugins/datetimepicker/
    $('.datepicker').datetimepicker({
        timepicker: false,
        format: 'Y-m-d'
    });

    // Push to Bento Button - Banners
    $('.call-bento-recache').on('click', function () {
        $.ajax({
            type: 'GET',
            url: '/banners/updateBento/'
        }).success(function () {
            $('#update-bento').closeModal();
            $('#update-bento-successful').openModal();
        });
    });

    // Lazy load banner images
    $('.lazy').lazyload();

    // Banner Seasons Filter - Mix-it-up plugin initialize
    $('#current-images').mixItUp();

})();
