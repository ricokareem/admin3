Dropzone.options.imageDropzone = {
    dictDefaultMessage: 'Create Banner',
    previewsContainer: '#image-preview',
    thumbnailWidth: 240,
    thumbnailHeight: 170,
    autoProcessQueue: true,
    addRemoveLinks: false,
    maxFilesize: 200 / 1024.0,
    init: function() {
        this.on('success', function( file, res ) {
            local_data = JSON.parse(res);
            local_file = file;
            $('#new-image-form #image_id').attr('value', local_data.id);
            $('#new-image-form #image_url').attr('value', local_data.full_image_path);
        });

        this.on('addedfile', function(file) {
            $('#image-dropzone').addClass('hide').removeClass('show');
        });

        this.on('thumbnail', function() {
            $('#image-preview').addClass('show').removeClass('hide');
        });
    }
}
