// Delete Flag button link
    $('#flags-table').on('click', 'a.deleteflag', deleteFlag);

function deleteFlag(event) {
    event.preventDefault();

    // Pop up a confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this flag?');

    // Check and make sure the flag confirmed
    if (confirmation === true) {
            flagId = $(this).attr('rel');
            $.ajax({
                type: 'DELETE',
                url: '/flags/delete/' + flagId
            }).done(function( response ) {

                // Check for a successful (blank) response
                if (response.msg === '') {
                }
                else {
                        alert('Error: ' + response.msg);
                }
                // Remove flag from the DOM
                $('.'+flagId).remove();
            });
    }
    else {
        return false;
    }
}
