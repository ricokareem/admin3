$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    defaultView: 'month',
    eventColor: '#263238',
    eventOrder: ['name', 'start'],
    events: function(start, end, timezone, callback) {
        $.ajax({
            url: '/flags',
            success: function(doc) {
                var events = [];
                $(doc).each(function() {
                    events.push({
                        _id: $(this).attr('_id'),
                        title: $(this).attr('name'),
                        start: $(this).attr('valid_from'),
                        end: $(this).attr('valid_to')
                    });
                });
                callback(events);
            }
        });
    },
    editable: true,
    eventRender: function (event, element) {
        element.attr('href', 'javascript:void(0);');
        element.click(function() {
            $('#editFlagModal-'+ event._id).openModal();
        });
    }
});
