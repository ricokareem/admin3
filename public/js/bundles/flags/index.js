require('./delete.js');
require('./fullcalendar.configs.js');
require('../../../less/flags.less');
require('../shared');


(function () {

    // use jQuery datetimepicker instead of Materialize Datepicker
    // http://xdsoft.net/jqplugins/datetimepicker/
    $('.datepicker').datetimepicker({
        format: 'Y-m-d\\TH:i:sO'
    });

    // Push to Bento Button - Flags
    $('.call-bento-recache').on('click', function () {
        $.ajax({
            type: 'GET',
            url: '/flags/updateBento/'
        }).success(function () {
            $('#update-bento').closeModal();
            $('#update-bento-successful').openModal();
        });
    });

})();
