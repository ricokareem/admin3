module.exports = {
    entry: {
        banners: "./public/js/bundles/banners/index.js",
        lite: "./public/js/bundles/lite/lite.js",
        flags: "./public/js/bundles/flags/index.js"
    },
    output: {
        path: __dirname,
        filename: "./public/js/bundle-[name].js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.less$/, loader: "style!css!less" }
        ]
    }
};
