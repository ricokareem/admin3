'use strict';

const DEFAULT_USER_TYPE = 'YP';

var format = require('util').format;
var config = require('config');
var request = require('request');
var fs = require('fs');

function bind(context) {

    function upload( data, callback ) {

        /* Example call:
            http://stage-yup.int.yp.com/blob
                ?api_key=n93RcH98m19lEb&metadata%5Buser_type%5D=AdminTool&metadata%5Buser_name%5D=admint_tool
        */
        var url = format('%s/blob', config.YUP.URL);

        var metadata = {
            user_type: 'AdminTool',
            user_name: 'admint_tool'
        };

        var options = {
            metadata: metadata,
            api_key: config.YUP.API_KEY
        };
        var params = {
            body: data,
            qs: options
        };
        request.put(url, params, callback);

    }

    return {
        upload: upload,
        _request: request
    };
}

module.exports = bind;
