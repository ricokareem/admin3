'use strict';

var format = require('util').format;
var config = require('config');
var request = require('request');
var fs = require('fs');

function bind(context) {

    function recache( req, callback ) {

        /* Example call:
            http://qa5-rservices.int.yp.com:7002
                /consumer/admin3/settings/hot/homepage_banners?app_id=WEB&recache=true
            or
            http://qa5-rservices.int.yp.com:7002
                /consumer/admin3/settings/hot/all_purpose_flag?app_id=WEB&recache=true
        */
        var url = format('%s/consumer/admin3/settings/hot/homepage_banners', config.BENTO.URL);
        // var url = format('%s/consumer/admin3/settings/hot/all_purpose_flag', config.BENTO.URL);

        var options = {
            app_id: 'WEB',
            recache: true
        };

        var params = {
            qs: options
        };

        request.get(url, params, callback);

    }

    return {
        recache: recache,
        _request: request
    };
}

module.exports = bind;
