function upload(req, callback) {
    var photo={};

    req.body={};
    req.pipe(req.busboy);
    req.busboy.on('field', function (key, value, keyTruncated, valueTruncated) {
        req.body[key]=value;
    });

    req.busboy.on('file', function (fieldname, file, filename, encoding, type) {
        photo.filename=filename;
        photo.type=type;
        var chunks=[];
        file.on('data', function (chunk) {
            chunks.push(chunk);
        });
        file.on('end', function () {
            photo.data=Buffer.concat(chunks);
        });

    });

    req.busboy.on('finish', function () {
        callback(photo);
    });

}
module.exports = upload;
