'use strict';

var bentoService    = require('app/services/bento'),
    monk            = require('monk'),
    revalidator     = require('revalidator'),
    config          = require('config'),
    format          = require('util').format;

/* MongoDB and Monk Configuration */
var flagsdbConfig = format('%s:%s@%s:%s/flagsdb',
    config.MONGO.USERNAME,
    config.MONGO.PASSWORD,
    config.MONGO.HOST,
    config.MONGO.PORT);
var flagsdb = monk(flagsdbConfig);
var collection = flagsdb.get('flags');


function index(req, res) {
    collection.find({},{},function(e,docs){
        res.render('flags/index', { flags: docs, title: 'All Purpose Flags' });
    });
}

function json(req, res) {
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
}

function create(req, res) {
    collection.insert(req.body, function(err, result){
        res.redirect(
            (err === null) ? '/flags/index' : ''
        );
    });
}

function remove(req, res) {
    var id = req.params.id;
    collection.remove({ '_id' : id }, function(err) {
        res.send((err === null) ? { msg: '' } : { msg: err });
    });
}

function edit(req, res) {
    var id            = req.params.id,
        fields        = req.body,
        fieldToUpdate = {};

    for (var name in fields) {
        fieldToUpdate[name] = fields[name];
    }

    collection.findAndModify(
        {
            'query': { '_id' : id },
            'update': { '$set': fieldToUpdate,
            },
            'options': { 'new': false, 'upsert': false }
        },
        function(err, result) {
            if (err) {
                console.error(err);
                res.status(500).render('error',
                {
                    message: err.message,
                    error: err
                });
            }
            res.redirect('/flags/index');
        }
    );

}

function updateBento(req, res) {
    var bento = bentoService(req);

    bento.recache(req, function (err, result) {
        if (err) {
            console.error(err);
            res.status(500).render('error',
            {
                message: err.message,
                error: err
            });
        }
        res.redirect('/flags/index');
    });
}


module.exports = {
    index: index,
    json: json,
    create: create,
    remove: remove,
    edit: edit,
    updateBento: updateBento
};
