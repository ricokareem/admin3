'use strict';

var yupService      = require('app/services/yup'),
    bentoService    = require('app/services/bento'),
    uploadHelper    = require('app/helpers/upload'),
    config          = require('config'),
    monk            = require('monk'),
    format          = require('util').format;

/* MongoDB and Monk Configuration */
var bannersdbConfig = format('%s:%s@%s:%s/bannersdb',
    config.MONGO.USERNAME,
    config.MONGO.PASSWORD,
    config.MONGO.HOST,
    config.MONGO.PORT);
var bannersdb = monk(bannersdbConfig);
var collection = bannersdb.get('banners');


function index(req, res) {
    collection.find({},{},function(e,docs){
        res.render('banners/index', { banners: docs, title: 'Homepage Banner Images' });
    });
}

function json(req, res) {
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
}

/* POST to create banner */
function create(req, res) {
    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
}

function remove(req, res) {
    var id = req.params.id;
    collection.remove({ '_id' : id }, function(err) {
        res.send((err === null) ? { msg: '' } : { msg: err });
    });
}

/* POST to update banner */
function edit(req, res) {
    var id            = req.params.id,
        fields        = req.body,
        fieldToUpdate = {};

    for (var name in fields) {
        fieldToUpdate[name] = fields[name];
    }

    collection.findAndModify(
        {
            'query': { '_id' : id },
            'update': { '$set': fieldToUpdate,
            },
            'options': { 'new': false, 'upsert': false }
        },
        function(err, result) {
            if (err) {
                console.error(err);
                res.status(500).render('error',
                {
                    message: err.message,
                    error: err
                });
            }
            res.redirect('/banners/index');
        }
    );
}

/* POST to upload banner image to yup */
function upload(req, res) {
    uploadHelper(req, function (photo) {

        var yup = yupService(req);

        if (!photo.data) {
            return res.sendStatus(400);
        }

        yup.upload( photo.data, function (err, result) {
            if (err) {
                console.error(err);
                res.status(400).render('error',
                {
                    message: err.message,
                    error: err
                });
            }
            res.send(result.body);
        });
    });
}

function updateBento(req, res) {
    var bento = bentoService(req);

    bento.recache(req, function (err, result) {
        if (err) {
            console.error(err);
            res.status(500).render('error',
            {
                message: err.message,
                error: err
            });
        }
        res.redirect('/banners/index');
    });
}

module.exports = {
    index: index,
    json: json,
    create: create,
    remove: remove,
    edit: edit,
    upload: upload,
    updateBento: updateBento
};
