################################################################################
#
#  Makefile tips:
#
#  1.) Commands starting with '@' are not echoed.
#  2.) In task comments are echoed by default.
#  3.) In task comments starting with '@' (e.g. '@# foo') are not echoed.
#  4.) Environment variables are accessable via '$(ENV_VAR_NAME)'.
#
#	Example:
#
#	#file: makefile
#	test_var:
#		echo "$(FOOBAR)"
#
#	$ make test_var FOOBAR=foobar
#	echo "foobar"
#	foobar
#	$
#
#  5.) Commands starting with '-' will not exit make if command fails.
#
################################################################################

NODE_ENV ?= dev

NODE_EXEC = NODE_PATH=.:$(NODE_PATH) NODE_ENV=$(NODE_ENV)

NODE_BIN   = ./scripts/node
NODE_MODS  = ./node_modules/.bin
folders    = yp,views,app.js,profile,apps
ignore     = --ignore public\
			 --ignore node_modules\
			 --ignore db\
			 --ignore makefiles\
			 --ignore package.json\
			 --ignore test\
			 --ignore scripts\

default: test

console: node_version_check
	# starting node console with $(NODE_ENV)
	$(NODE_EXEC) $(NODE_BIN)

debug: node_version_check
	# running app in debug mode with '$(NODE_ENV)'
	$(NODE_EXEC) $(NODE_BIN) --prof --debug app.js

runapp: node_version_check
	# running app with '$(NODE_ENV)'
	$(NODE_EXEC) ./node_modules/.bin/nodemon -e js,jade,yml $(ignore)

run: node_version_check
	./scripts/run

test: node_version_check
	$(NODE_EXEC) ./node_modules/.bin/mocha --harmony --recursive test

node_version_check: .PHONY
	-@bash ./scripts/node_version_check.sh


.PHONY:
