SAMPLE BANNERS ADMIN TOOL - ExpressJS, MongoDB

* Node version: 4.1.1+
* NPM version: 2.14.4+

INSTALLATION

1. Install Mongo: `brew install mongodb` - [https://www.mongodb.org/]
1. `npm install`


RUNNING THE APP IN DEV MODE - terminal windows 1. mongoDB, 2. app server, 3. your workspace window, 4. webpack watcher(opt)

1. If it doesn't already exist, create db folder on project root: `mkdir db`
1. In terminal 1, type: `mongod --dbpath db` to start mongoDB
2.
    Option 1
    1. In terminal 2, type: `make run`

    Option 2
    1. Instead of `make run`, in terminal window 2, type: `NODE_ENV=<env> npm start` to start the app running in dev mode on port 3000 (just typing `npm start` will default to dev).
    1. In terminal window 3, if you would like to keep the .js & .less file compiler listening for updates in the background, type `webpack --progress --colors --watch`

TESTING (Work in Progress)

1. `make test`
    * this will run: `NODE_ENV=dev ./node_modules/.bin/mocha --harmony --recursive test`
    * you can also run single tests: `NODE_ENV=dev ./node_modules/.bin/mocha --harmony test/unit/services`

DEPLOYING THE APP

    (PLACEHOLDER FOR DOCKER AND AUTOMATION)

RUNNING THE APP IN PRODUCTION (Work In Progress)

1. If it doesn't already exist, create db folder on project root: `mkdir db`
1. `mongod --dbpath db`
1. `NODE_ENV=environment npm start` (_where environment = **dev, qa, prod**, etc_). app will run on port 3000 by default

ROUTES

    /               = Main Web App (same as /banners/index)
    /banners/index  = Homepage Banner Images
    /banners        = banners.json file (automatically generated)
    /flags/index    = All Purpose Flags
    /flags          = flags.json file (automatically generated)
    /lite           = A lighter version of the app (not ready yet)

NOTE:
This app is in a very raw experimental state.

Third-party client dependency tree as of 2016.04.01

    ├─┬ Materialize#0.97.6
    │ └── jquery#2.2.3 (3.0.0-beta1 available)
    ├─┬ datetimepicker#2.4.5 (latest is 2.5.3)
    │ └── jquery#2.2.3 (3.0.0-beta1 available)
    ├── dropzone#4.3.0
    ├─┬ fullcalendar#2.4.0 (latest is 2.7.1)
    │ ├── jquery#2.2.3 (3.0.0-beta1 available)
    │ └── moment#2.12.0 (2.13.0 available)
    ├── jquery_lazyload#1.9.7
    ├── material-design-icons#2.2.3
    └─┬ mixitup#2.1.11
      └── jquery#2.2.3 (3.0.0-beta1 available)
