'use strict';

var yaml    = require('js-yaml'),
    format  = require('util').format,
    fs      = require('fs');

var nodeEnv = process.env.NODE_ENV || 'dev';
var configFile = fs.readFileSync(format('./config/environments/%s.yml', nodeEnv), 'utf8');
var configEnv = yaml.safeLoad(configFile);

function getenv(key, fallback) {

    if (configEnv[key]) {
        return configEnv[key];
    }

    // Must be configured somewhere.
    if (!configEnv.hasOwnProperty(key) && undefined === fallback) {
        throw new Error('getenv: unable to fetch ' + key);
    }

    // Configured? Otherwise use fallback.
    return (configEnv.hasOwnProperty(key) && typeof configEnv[key] !== 'undefined') ? configEnv[key] : fallback;
}

module.exports = {
    PORT: {
        HTTP: getenv('HTTP_PORT', 3000)
    },

    BENTO: {
        URL: getenv('BENTO_URL')
    },

    YUP: {
        URL: getenv('YUP_URL'),
        API_KEY: getenv('YUP_API_KEY')
    },

    MONGO: {
        HOST: getenv('MONGO_HOST'),
        PORT: getenv('MONGO_PORT'),
        USERNAME: getenv('MONGO_USERNAME'),
        PASSWORD: getenv('MONGO_PASSWORD')
    }

};
