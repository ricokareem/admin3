#FROM centos:6.6
FROM node:argon

ENV BASEDIR /tmp/admin3
WORKDIR ${BASEDIR}
# Make ssh dir
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
ADD np_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
RUN ssh-keyscan stash.int.yp.com >> /root/.ssh/known_hosts

RUN npm install -g webpack

ADD package.json $BASEDIR/package.json
RUN npm install

ADD . $BASEDIR

EXPOSE 3000 3000
ENTRYPOINT $BASEDIR/entrypoint.sh
