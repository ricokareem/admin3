'use strict';

var express         = require('express'),
    busboy          = require('connect-busboy'),
    path            = require('path'),
    favicon         = require('serve-favicon'),
    logger          = require('morgan'),
    cookieParser    = require('cookie-parser'),
    bodyParser      = require('body-parser'),
    monk            = require('monk'),
    util            = require('util'),
    fs              = require('fs'),
    config          = require('config'),
    format          = require('util').format;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'jade');

// Initializing system variables
app.set('config', config);

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboy());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/node_modules',  express.static( path.join(__dirname, '/node_modules')));


/* Setup Handlers */
var flags = require('app/handlers/flags');
var banners = require('app/handlers/banners');

app.get('/flags', flags.json);
app.get('/flags/index', flags.index);
app.post('/flags/create', flags.create);
app.post('/flags/edit/:id', flags.edit);
app.delete('/flags/delete/:id', flags.remove);
app.get('/flags/updateBento', flags.updateBento);

app.get('/banners', banners.json);
app.get('/banners/index', banners.index);
app.post('/banners/create', banners.create);
app.post('/banners/edit/:id', banners.edit);
app.delete('/banners/delete/:id', banners.remove);
app.get('/banners/updateBento', banners.updateBento);
app.post('/upload', banners.upload);


/* Lite Test Page */
app.get('/lite', function(req, res, next) {
    res.render('banners/lite', { title: 'Homepage Lite' });
});

/* Main Page */
app.get('/', function(req, res, next) {
    res.redirect('banners/index');
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// other error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'dev') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
