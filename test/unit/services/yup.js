'use strict';

var fs     = require('fs');
var sinon  = require('sinon');
var should = require('should');
var assert = require('assert');
var config = require('../../../config');

var yup    = require('../../../app/services/yup')({'bento_mock_object': 'foo'});


describe('yup', function() {
    var callback = function() {};
    var basePath = config.YUP.URL;

    before(function() {
        ['get','put','post','del'].forEach(function(meth) {
            sinon.stub(yup._request, meth);
        });
    });

    after(function() {
        ['get','put','post','del'].forEach(function(meth) {
            yup._request[meth].restore();
        });
   });

    it('upload', function(done) {
        var url = basePath+'/blob';

        var _fs_readFile = fs.readFile;
        fs.readFile = function(path, callback) {
            callback(null, 'foo');
        };

        var data = { 'picture': 'foo' };
        var expected = {
            body: { 'picture': 'foo' },
            qs: { api_key: config.YUP.API_KEY }
        };

        yup.upload( data, callback );
        assert(yup._request.put.calledWithMatch(url, expected));

        fs.readFile = _fs_readFile;
        done();
    });

});

